import './App.css';
import Homepages from './pages/homepages';


function App() {
  return (
    <div className="App">
      <Homepages />
    </div>
  );
}

export default App;
